const MESSAGE_HIST_LENGTH = 10;
const DATAFILE = "./data/data.json";

const fs = require("fs");

const express = require("express");
const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http, { maxHttpBufferSize: 1e9 });

const FixedStack = require("./helpers/stack");

const messageStack = new FixedStack(MESSAGE_HIST_LENGTH);

const usersList = [];

fs.readFile(DATAFILE, (err, data) => {
    if (!!err) console.error(err);
    else
        try {
            messageStack.load(JSON.parse(data).stack);
        } catch (error) {
            console.error(error);
        }
});

app.use(express.static("public"));

io.on("connection", socket => {
    console.log("a user connected");

    socket.on("connected", data => {
        io.emit("connected", data);
        socket.username = data.username;
        usersList.push(data.username);

        messageStack.stack.forEach(msg => {
            msg.isHistoryMsg = true;
            switch (msg.msgType) {
                case "chat":
                    socket.emit("chat message", msg);
                    break;
                case "reply":
                    socket.emit("chat message", msg);
                    break;
                case "img":
                    socket.emit("encImg", msg);
                    break;
                case "audio":
                    socket.emit("audio message", msg);
                    break;
                default:
                    break;
            }
        });
    });

    socket.on("disconnect", () => {
        if (socket.username) {
            usersList.pop(usersList.indexOf(socket.username));
            io.emit("disconnected", { username: socket.username });
            console.log("user disconnected");
            fs.writeFile(
                "./data/data.json",
                JSON.stringify(messageStack),
                console.error
            );
        } else {
            console.log("A ghost left. The chat is no longer haunted.");
        }
    });

    // Handle chat message events
    socket.on("chat message", msg => {
        io.emit("chat message", msg);
        delete msg.isNewMessage;
        messageStack.push(msg);
    });

    socket.on("recording audio", meta => {
        io.emit("recording audio", meta);
    });

    socket.on("stopped recording audio", meta => {
        io.emit("stopped recording audio", meta);
    });

    socket.on("typing", meta => {
        io.emit("typing", meta);
    });

    socket.on("audio message", msg => {
        io.emit("audio message", msg);
        delete msg.isNewMessage;
        messageStack.push(msg);
    });

    socket.on("just sent img", meta => {
        io.emit("just sent img", meta);
    });
    socket.on("just sent audio message", meta => {
        io.emit("just sent audio message", meta);
    });

    socket.on("encImg", msg => {
        io.emit("encImg", msg);
        delete msg.isNewMessage;
        messageStack.push(msg);
    });

    socket.on("img", msg => {
        io.emit("img", msg);
        delete msg.isNewMessage;
        messageStack.push(msg);
    });

    socket.on("clear chat", () => {
        // if (key === process.env.ADMIN_KEY) {
        messageStack.clear();
        fs.unlink(DATAFILE, err => {
            if (err) io.emit("clear chat fail", err);
            else io.emit("clear chat success");
        });
        // } else io.emit("clear chat fail", "Error: Incorrect admin key")
    });

    socket.on("get users list", () => {
        socket.emit("users list", usersList);
    });
});

const PORT = process.env.PORT || 3000;

http.listen(PORT, () => {
    console.log(`listening on *:${PORT}`);
});
