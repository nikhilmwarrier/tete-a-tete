# Tête-á-Tête

Encrypted chat application.

Supports encrypted photo sharing and voice messaging.

-   On opening the app, you will be prompted to enter a username and a key, which will be used to encrypt/decrypt all messages with AES.
-   Only users who enter the same key as you will be able to decrypt and see your messages.
-   This key is _never_ sent to the server. It is not even saved in the browser's cache and must be re-entered every time.

## Development

Requires Node.js and npm.

1. Clone the repo

```bash
$ git clone 'https://codeberg.org/nikhilmwarrier/tete-a-tete'
$ cd tete-a-tete
```

2. Install dependencies

```bash
$ npm install
```

3. Run

```bash
$ npm run serve
```
