class FixedStack {
  constructor(size) {
    this.size = size || 1;
    this.stack = [];
    return this;
  }

  push(element) {
    if (this.stack.length >= this.size) {
      this.stack.shift(element);
    }
    this.stack.push(element);
    return this
  }

  load(stack) {
    this.stack = stack
    return this
  }

  clear() {
    this.stack = []
    return this
  }
}

module.exports = FixedStack
