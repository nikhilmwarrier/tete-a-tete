// Time blocker
const blocker = document.querySelector("#time-blocker");
const countdown = blocker.querySelector(".countdown");
function checkBlocker() {
  const date = new Date();
  const hour = date.getHours();

  if (hour >= 22 || hour <= 2) {
    blocker.close();
  } else {
    const hh = (22 - 1 - date.getHours()).toString().padStart(2, "0");
    const mm = (59 - date.getMinutes()).toString().padStart(2, "0");
    const ss = (59 - date.getSeconds()).toString().padStart(2, "0");
    countdown.innerText = `${hh}:${mm}:${ss}`;
  }
}

checkBlocker();
setInterval(() => {
  checkBlocker();
}, 1000);

const sanitize = (text) => text.replace(/[^a-z0-9-]/gi, "");

const usernameFromLocalStorage = localStorage.getItem("username");

let username = "";
let decryptionKey = "";

let settings = JSON.parse(localStorage.getItem("settings")) || {
  useMonospaceEditor: "off",
  showMessageSendButton: "off",
};

const settingsForm = document.getElementById("settings-form");

const loginDialog = document.querySelector("#login-dialog");
const loginForm = loginDialog.querySelector("form");

const usernameInput = loginForm.querySelector("#username");
const keyInput = loginForm.querySelector("#decryption-key");

if (!!usernameFromLocalStorage) {
  usernameInput.value = sanitize(usernameFromLocalStorage);
  keyInput.focus();
} else usernameInput.focus();

const socket = io();

const messageHistory = [];

// FOR DEBUG
// loginDialog.classList.add("closed");
// username = localStorage.getItem("username") || "tempuser-0001";
// decryptionKey = "testkey";
// socket.emit("connected", { username });

settingsForm.addEventListener("submit", (e) => {
  e.preventDefault();
  settingsData = new FormData(settingsForm);
  settings = {};
  for (const [key, value] of settingsData) {
    settings[key] = value;
  }
  applySettings();
  closeSettingsModal();
});

loginForm.addEventListener("submit", (e) => {
  e.preventDefault();
  username = sanitize(usernameInput.value);
  decryptionKey = keyInput.value;
  loginDialog.classList.add("closed");
  if (username.length === 0) window.location.reload();
  localStorage.setItem("username", username);

  // Signal successful connection
  socket.emit("connected", { username });
});

// FOR DEBUG
// const username = `User-${Date.now()}`;
// const decryptionKey = "tempkey";

let replyTo = "";

const messages = document.getElementById("messages");
const messageForm = document.getElementById("message-form");
const messageSendButton = document.getElementById("message-send-button");
const replyIndicator = messageForm.querySelector(".replying-to");
const messageInput = document.getElementById("message-input");
const imgInput = document.getElementById("file-input");
const imgUploadPreview = document.querySelector("img.img-preview");
// const recordToggle = document.querySelector("#recording-toggle");
const recordButton = document.querySelector("#recording-button");
const base64SizeInKB = (base64length) => (base64length * 0.75 - 2) / 1000;
const encryptData = (data) =>
  CryptoJS.AES.encrypt(data, decryptionKey).toString();
const decryptData = (data) =>
  CryptoJS.AES.decrypt(data, decryptionKey).toString(CryptoJS.enc.Utf8);

function downloadStringAsText(text, fileType, fileName) {
  var blob = new Blob([text], { type: fileType });

  var a = document.createElement("a");
  a.download = fileName;
  a.href = URL.createObjectURL(blob);
  a.dataset.downloadurl = [fileType, a.download, a.href].join(":");
  a.style.display = "none";
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
  setTimeout(function () {
    URL.revokeObjectURL(a.href);
  }, 1500);
}

const dataURItoBlob = (dataURI) => {
  const mime = dataURI.split(",")[0].split(":")[1].split(";")[0];
  const binary = atob(dataURI.split(",")[1]);
  let array = [];
  for (var i = 0; i < binary.length; i++) {
    array.push(binary.charCodeAt(i));
  }
  return new Blob([new Uint8Array(array)], { type: mime });
};

function getMessage(msgID) {
  return messageHistory.find((msg) => msg.id == msgID) || null;
}

const getTimestamp = (unixtimestamp) => {
  date = new Date(unixtimestamp);
  today = new Date();

  let finalDateString = "";

  // Add date if the message is not from the same day
  if (date.toDateString() !== today.toDateString()) {
    finalDateString += date.toDateString();
  }
  finalDateString +=
    ` ${date.getHours()}`.padStart(2, "0") +
    ":" +
    `${date.getMinutes()}`.padStart(2, "0");

  return finalDateString;
};

function parseSlashCommands(command) {
  console.log(command);
  switch (command.trim()) {
    case "/help":
      alert(`
      Slash commands help:
      /clearchat - Clear chat from server.
      /downloadchat - Download all chat messages in current session
      /users - Get users list (NOTE: Doesn't work properly)
      `);
      break;
    case "/clearchat":
      socket.emit("clear chat");
      break;
    case "/downloadchat":
      downloadMessages();
      break;
    case "/users":
      socket.emit("get users list");
      break;
    default:
      alert("Unknown command");
      break;
  }
}

function createNotification(text) {
  // constrain notification text size to prevent crashes
  const _text = text.length > 300 ? text.slice(0, 297) + "..." : text;
  Push.create("Tête-á-Tête", {
    body: _text,
    icon: "./icons/128x128.png",
    // timeout: 4000,
    serviceWorker: "./sw.js",
    link: "/index.html",
    onClick: function () {
      window.focus();
      this.close();
    },
  });
}

function handleReply(e) {
  const msgID = e.currentTarget.id;
  // if (!msgID || msgID.length !== 21) return;
  replyTo = msgID;
  const msg = getMessage(msgID);
  if (!msg) return;
  if (e.currentTarget.querySelector(".reply-section")?.contains(e.target))
    return;
  let content = "";
  if (msg.type === "chat" || msg.type === "reply") {
    content = msg.content;
  } else if (msg.type === "img") {
    content = "<Image>";
  } else if (msg.type === "audio") {
    content = "<Voice Message>";
  }
  replyIndicator.querySelector(".msg-preview .sender").innerText = msg.sender;
  replyIndicator.querySelector(".msg-preview .content").innerText = content;
  replyIndicator.classList.remove("hidden");
  messageInput.focus();
}

function cancelReply() {
  replyTo = "";
  replyIndicator.classList.add("hidden");
}

function applySettings() {
  if (settings.useMonospaceEditor === "on") {
    messageInput.classList.add("monospace-editor");
  } else {
    messageInput.classList.remove("monospace-editor");
  }

  if (settings.showMessageSendButton === "on") {
    messageForm.dataset.buttons = "3";
    messageSendButton.classList.remove("hidden");
  } else {
    messageForm.dataset.buttons = "2";
    messageSendButton.classList.add("hidden");
  }
  localStorage.setItem("settings", JSON.stringify(settings));
}

// Apply settings on load
applySettings();

// Deselect highlighted messages
// window.addEventListener("hashchange", () => {
//     setTimeout(() => {
//         window.location.hash = "";
//     }, 2000);
// });

function appendMessage(
  msgType,
  content,
  sender,
  id,
  timestamp,
  replyTo,
  isNewMessage
) {
  if (content.toString().trim() === "") return;

  messageHistory.push({
    id,
    type: msgType,
    content,
    sender,
    timestamp,
    replyTo,
  });
  if (["chat", "img", "audio", "info", "reply"].includes(msgType)) {
    const template = document.querySelector(`#template-${msgType}-message`);

    const messageTemplate = template.content.cloneNode(true);

    if (!!id) messageTemplate.querySelector(".message").id = id;

    if (msgType !== "info") {
      messageTemplate.querySelector(".sender").innerText = sender;
      if (sender === username)
        messageTemplate.querySelector(".message").classList.add("own-message");
      else
        messageTemplate
          .querySelector(".message")
          .classList.add("not-own-message");
    } else {
      messageTemplate.querySelector(".message").classList.add("info");
    }

    if (!isNewMessage)
      messageTemplate.querySelector(".message").classList.add("from-history");
    messageTemplate.querySelector(".timestamp").innerText = !!timestamp
      ? getTimestamp(timestamp)
      : "";

    if (["chat", "reply", "info"].includes(msgType)) {
      const contentDiv = messageTemplate.querySelector(".message-content");
      // if (isOnlyOneEmoji(content) || content === "🫠" || content === "🥲")
      //     contentDiv.classList.add("emoji");
      contentDiv.innerHTML = DOMPurify.sanitize(marked.parse(content));

      if (msgType === "reply") {
        const replySection = messageTemplate.querySelector(".reply-section");
        const replyMsg = getMessage(replyTo);
        replySection.querySelector("a").href = `#${replyTo}`;
        replySection.querySelector(".sender").innerText = replyMsg.sender;
        if (replyMsg.type === "chat" || replyMsg.type === "reply") {
          replySection.querySelector(".content").innerText = replyMsg.content;
        } else if (replyMsg.type === "img") {
          replySection.querySelector(".content").innerText = "<Image>";
        } else if (replyMsg.type === "audio") {
          replySection.querySelector(".content").innerText = "<Voice Message>";
        }
      }
    } else if (msgType === "img") {
      img = messageTemplate.querySelector("img.message-content");
      img.setAttribute("src", content);
      img.setAttribute("alt", `Image from ${sender}`);
    } else if (msgType === "audio") {
      audioEl = messageTemplate.querySelector("audio");
      decryptedAudioURL = window.URL.createObjectURL(dataURItoBlob(content));
      audioEl.setAttribute("src", decryptedAudioURL);
    }

    messageTemplate
      .querySelector(".message")
      .addEventListener("click", handleReply);

    messages.appendChild(messageTemplate);
  } else {
    console.error("No message type specified.");
  }

  messages.scrollTop = messages.scrollHeight;
}

socket.on("connected", (data) => {
  appendMessage("info", `${data.username} joined the chat.`);
  if (document.visibilityState !== "visible") {
    createNotification(`${data.username} joined the chat.`);
  }
});

socket.on("disconnected", (data) => {
  appendMessage("info", `${data.username} left the chat.`);
  if (document.visibilityState !== "visible") {
    createNotification(`${data.username} left the chat`);
  }
});

// Handle incoming chat messages
socket.on("chat message", (data) => {
  const decryptedMsg = decryptData(data.encryptedMsg);
  if (!!decryptedMsg) {
    appendMessage(
      data.msgType,
      decryptedMsg,
      data.username,
      data.id,
      data.timestamp,
      data?.replyTo,
      data?.isNewMessage
    );
  } else {
    appendMessage(
      "info",
      `${data.username} sent a message, but it couldn't be decrypted.\
       Check if your keys match.`
    );
  }
  if (document.visibilityState !== "visible") {
    createNotification(`${data.username}: ${decryptedMsg}`);
  }
});

// the thing with this variable sort-of works for some reason, but it is wonky
let isTyping = false;
socket.on("typing", (data) => {
  if (data.username !== username) {
    isTyping = true;
    console.log(data.username + " is typing");
    indicator = document.querySelector(".indicator-typing");
    indicator.classList.add("visible");
    indicator.querySelector(".username").innerText = data.username;

    setTimeout(() => {
      if (isTyping === false) {
        indicator.classList.remove("visible");
        indicator.querySelector(".username").innerText = "";
      } else {
        isTyping = false;
      }
    }, 2500);
  }
});

socket.on("recording audio", (data) => {
  if (data.username !== username) {
    console.log(data.username + " is recording audio");
    indicator = document.querySelector(".indicator-recording-audio");
    indicator.classList.add("visible");
    indicator.querySelector(".username").innerText = data.username;
  }
});

socket.on("stopped recording audio", (data) => {
  if (data.username !== username) {
    console.log(data.username + " stopped recording audio");
    indicator = document.querySelector(".indicator-recording-audio");
    indicator.classList.remove("visible");
    indicator.querySelector(".username").innerText = "";
  }
});

socket.on("just sent audio message", (meta) => {
  appendMessage("info", `${meta.username} sent an audio message.`);
});

socket.on("audio message", (data) => {
  const decryptedAudioBase64 = decryptData(data.encryptedAudio);
  appendMessage(
    "audio",
    decryptedAudioBase64,
    data.username,
    data.id,
    data.timestamp,
    data?.replyTo,
    data?.isNewMessage
  );
});

socket.on("just sent img", (meta) => {
  appendMessage("info", `${meta.username} sent an image.`);
});

socket.on("encImg", (data) => {
  const decryptedImg = decryptData(data.encryptedImg);
  appendMessage(
    "img",
    decryptedImg,
    data.username,
    data.id,
    data.timestamp,
    data?.replyTo,
    data?.isNewMessage
  );
});

socket.on("img", (data) => {
  const imgElement = document.createElement("img");
  imgElement.src = data.img;
  imgElement.width = 200;
  messages.appendChild(imgElement);
  messages.scrollTop = messages.scrollHeight;
});

socket.on("clear chat fail", (error) => {
  alert("Clear chat failed. See console for errors.");
  console.error(error);
});

socket.on("clear chat success", () => {
  alert("Successfully cleared chat history from the server.");
});

socket.on("users list", (usersList) =>
  alert("Current users:\n" + usersList.toString().replaceAll(",", "\n"))
);

// Send chat messages
messageForm.addEventListener("submit", (e) => {
  e.preventDefault();

  const message = messageInput.value;
  if (message.startsWith("/")) {
    parseSlashCommands(message);
    messageInput.value = "";
  } else {
    const encryptedMsg = encryptData(message);

    if (message.trim() !== "") {
      console.log(replyTo);
      if (!!replyTo) {
        socket.emit("chat message", {
          timestamp: Date.now(),
          id: nanoid(),
          username,
          msgType: "reply",
          replyTo,
          encryptedMsg,
          isNewMessage: true,
        });
        replyTo = "";
        cancelReply();
      } else {
        socket.emit("chat message", {
          timestamp: Date.now(),
          id: nanoid(),
          username,
          msgType: "chat",
          encryptedMsg,
          isNewMessage: true,
        });
      }
    }

    // imgUploadPreview.src might return truthy values even if the attr is empty
    const img = imgUploadPreview.getAttribute("src");

    if (!!img) {
      const encryptedImg = encryptData(img);
      console.log(
        `Encrypted: ${base64SizeInKB(encryptedImg.length)} KB
      Original: ${base64SizeInKB(img.length)} KB`
      );
      socket.emit("just sent img", { username });
      socket.emit("encImg", {
        timestamp: Date.now(),
        id: nanoid(),
        username,
        msgType: "img",
        encryptedImg,
        isNewMessage: true,
      });
      closeImgPreviewModal();
    }

    messageInput.value = "";
    imgInput.value = null;
    imgUploadPreview.src = "";
  }
});

messageInput.addEventListener("keyup", (e) => {
  // Check if ctrl or shift is pressed to handle newline insertion
  if (e.key === "Enter" && !e.ctrlKey && !e.shiftKey) {
    // Use backslashes to enter newlines.
    // Useful for devices without physical keyboards
    if (messageInput.value.trim().endsWith("\\")) {
      messageInput.value = messageInput.value.trim().slice(0, -1) + "  \n";
    } else messageSendButton.click();
  } else if (e.key === "Escape") cancelReply();
});

async function captureAudioStream() {
  const stream = await navigator.mediaDevices.getUserMedia({
    audio: {
      echoCancellation: true,
      noiseSuppression: true,
    },
  });
  return stream;
}

let mediaRecorder;
let isRecording = false;

if (!navigator.mediaDevices) recordButton.disabled = true;

async function recordAudioStream() {
  const stream = await captureAudioStream();
  mediaRecorder = new MediaRecorder(stream);

  let chunks = [];

  mediaRecorder.ondataavailable = (e) => {
    chunks.push(e.data);
  };

  mediaRecorder.onstop = (e) => {
    console.log(mediaRecorder.state);
    socket.emit("stopped recording audio", { username });
    sendAudioMessage(chunks);
  };

  mediaRecorder.start();
  isRecording = true;
  recordButton.classList.add("is-recording");
  socket.emit("recording audio", { username });
  console.log(mediaRecorder.state);
}

function stopRecording() {
  mediaRecorder.stream.getTracks().forEach((track) => track.stop());
  isRecording = false;
  recordButton.classList.remove("is-recording");
}

function sendAudioMessage(chunks) {
  const blob = new Blob(chunks, {
    type: "audio/ogg; codecs=opus",
  });
  const reader = new FileReader();
  reader.readAsDataURL(blob);
  reader.onloadend = () => {
    const base64audio = reader.result;
    chunks = [];
    blob.arrayBuffer().then((buf) => {
      socket.emit("just sent audio message", {
        username,
      });
      socket.emit("audio message", {
        username,
        timestamp: Date.now(),
        id: nanoid(),
        msgType: "audio",
        encryptedAudio: encryptData(base64audio),
        isNewMessage: true,
      });
    });
  };
}

recordButton.addEventListener("click", (e) => {
  if (isRecording) stopRecording();
  else recordAudioStream();
});

// typing indicator
messageInput.addEventListener("keydown", () => {
  console.log("typing");
  socket.emit("typing", { username });
});

function imageCompressionProgressBarHandler(percentage) {
  console.log("Compressing image:", percentage + "%");
  progressBarText = document.getElementById("compression-progress-bar-text");
  progressBar = document.getElementById("compression-progress-bar");
  progressIndicator = progressBar.querySelector(".loaded");
  progressBar.style.opacity = 1;
  progressBarText.style.opacity = 1;
  progressIndicator.style.width = `${percentage}%`;
  if (percentage === 100) {
    progressBar.style.opacity = 0;
    progressBarText.style.opacity = 0;
  }
}

function processImage() {
  const file = imgInput.files[0];

  if (!!file) openImgPreviewModal();

  if (file.size < 1000000) {
    previewImage(file);
  } else if (file.type !== "image/jpeg") {
    if (
      confirm(
        "You are about to upload a large non-jpeg image. Sending image without\
         compression. This may take time. Proceed?"
      )
    ) {
      previewImage(file);
    } else {
      closeImgPreviewModal();
      return;
    }
  } else {
    imageCompression(file, {
      maxSizeMB: 1,
      maxWidthOrHeight: 1920,
      useWebWorker: true,
      onProgress: imageCompressionProgressBarHandler,
    })
      .then((img) => {
        previewImage(img);
      })
      .catch(console.error);
  }
}

function previewImage(img) {
  const reader = new FileReader();
  reader.addEventListener(
    "load",
    // () => (imgUploadPreview.src = reader.result),
    () => (imgUploadPreview.src = reader.result),
    false
  );

  if (img) {
    reader.readAsDataURL(img);
  }
}

function downloadMessages() {
  const jsonMessageObject = {};
  let shouldDownloadMedia = false;
  if (
    confirm(
      "Download media too? Media will be saved as data URIs. May create files\
      large enough to crash most text editors."
    )
  ) {
    shouldDownloadMedia = true;
  }
  let i = 0;
  messageHistory.forEach((msg) => {
    msgCopy = { ...msg };
    if (!shouldDownloadMedia) {
      if (msgCopy.type === "img") msgCopy.content = "<Image>";
      if (msgCopy.type === "audio") msgCopy.content = "<Audio Message>";
    }
    jsonMessageObject[i] = msgCopy;
    i++;
  });
  timestamp = new Date();
  downloadStringAsText(
    JSON.stringify(jsonMessageObject, null, 4),
    "text/json",
    `tete-a-tete-messages-${timestamp.toISOString()}.json`
  );
}

function sendImage() {
  closeImgPreviewModal();
}

function cancelSendingImage() {
  imgUploadPreview.setAttribute("src", "");
  closeImgPreviewModal();
}

function openImgPreviewModal() {
  const modal = document.querySelector(".img-preview-modal");
  modal.style.top = "0";
}

function closeImgPreviewModal() {
  const modal = document.querySelector(".img-preview-modal");
  modal.style.top = "-100vh";
}

function openSettingsModal() {
  const modal = document.querySelector(".settings-modal-backdrop");
  modal.classList.add("open");
}

function closeSettingsModal() {
  const modal = document.querySelector(".settings-modal-backdrop");
  modal.classList.remove("open");
}

if (Push.Permission.has() === false)
  document.querySelector("button.request-notifications").style.visibility =
    "visible";
